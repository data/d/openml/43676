# OpenML dataset: NSE-Stocks-Data

https://www.openml.org/d/43676

## Structure

The dataset has the following file structure:

* `dataset/`
  * `tables/`
    * [`data.pq`](./dataset/tables/data.pq): Parquet file with data
  * [`metadata.json`](./dataset/metadata.json): OpenML description of the dataset
  * [`features.json`](./dataset/features.json): OpenML description of table columns
  * [`qualities.json`](./dataset/qualities.json): OpenML qualities (meta-features)

## Description

Context
The data is of National Stock Exchange of India.
The data is compiled to felicitate Machine Learning, without bothering much about Stock APIs.
Content
The data is of National Stock Exchange of India's stock listings for each trading day of 2016 and 2017.
A brief description of columns.
SYMBOL: Symbol of the listed company. 
SERIES: Series of the equity. Values are [EQ, BE, BL, BT, GC and IL] 
OPEN: The opening market price of the equity symbol on the date. 
HIGH: The highest market price of the equity symbol on the date. 
LOW: The lowest recorded market price of the equity symbol on the date. 
CLOSE: The closing recorded price of the equity symbol on the date. 
LAST: The last traded price of the equity symbol on the date. 
PREVCLOSE: The previous day closing price of the equity symbol on the date. 
TOTTRDQTY: Total traded quantity of the equity symbol on the date. 
TOTTRDVAL: Total traded volume of the equity symbol on the date. 
TIMESTAMP: Date of record. 
TOTALTRADES: Total trades executed on the day. 
ISIN: International Securities Identification Number. 
Acknowledgements
All data is fetched from NSE official site. 
https://www.nseindia.com/
Inspiration
This dataset is compiled to felicitate Machine learning on Stocks.

## Contributing

This is a [read-only mirror](https://gitlab.com/data/d/openml/43676) of an [OpenML dataset](https://www.openml.org/d/43676). Contribute any changes to the dataset there. Alternatively, [fork the dataset](https://gitlab.com/data/d/openml/43676/-/forks/new) or [find an existing fork](https://gitlab.com/data/d/openml/43676/-/forks) to contribute to.

You can use [issues](https://gitlab.com/data/d/openml/43676/-/issues) to discuss the dataset and any issues.

For more information see [https://datagit.org/](https://datagit.org/).

